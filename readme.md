# JAVA 9 Demo

Prerequisites:
- install JDK 9
- set environment variable `JAVA_9_HOME/bin/javac` to path to JDK-9 /bin directory

To compile (from project root dir) to `out` directory:
`
"$JAVA_9_HOME/bin/javac" -d out/ --module-source-path src $(find src -name "*.java")
`

To run:
`
"$JAVA_9_HOME/bin/java" --module-path out/no.itera.demo/ -m no.itera.demo/no.itera.demo.Main
`
